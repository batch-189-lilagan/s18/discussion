function sayMyName(name) {
    console.log('My name is ' + name);
};

sayMyName('Slim Shady');

sayMyName('Vice Ganda');
sayMyName('Coco Martin');


let myName = 'Earl';
sayMyName(myName);


let sumOfTwoNumbers = (num1, num2) => console.log(num1 + num2);

sumOfTwoNumbers(10,15);

function argumentFunction() {
    console.log('This is a function that was passed as an argument');
};

function parameterFunction(argumentFunction) {
    argumentFunction();
};
parameterFunction(argumentFunction);



// REAL WORLD APPLICATION
function addProductToCart() {
    console.log('Click me to add product to cart');
};

let addToCartBtn = document.querySelector('#add-to-cart-btn')
addToCartBtn.addEventListener('click', addProductToCart);

// function displayProductPage() {
//     console.log('Product page has been displayed.');
// };

const displayFullname = (firstName, middleInitial, lastName) => {
    console.log(`Your full name is: ${firstName} ${middleInitial} ${lastName}`);
}

displayFullname('Charles','A.','Lilagan')


function myNameIs() {
    return 'Charles Patrick Lilagan';
};

console.log(`Hello everyone! I'm ${myNameIs()}`);


function displayPlaerDetails(name, age, playerClass) {
    // console.log(`Player name: ${name}`);
    // console.log(`Player age: ${age}`);
    // console.log(`Player class: ${playerClass}`);
    let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`;

    return playerDetails;
}

console.log(displayPlaerDetails('Vice Ganda', 69, 'Centaur'));



